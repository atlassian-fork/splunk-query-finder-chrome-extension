const highlightedClass = "highlighted";
const highlightedClassSelector = "#confluence_links ." + highlightedClass + ":visible";
const iconSelector = ".search-name"

let currentSearchPhrase = "";

// --------------------------------- MODAL CREATION ----------------------------

$('body').append('<div id="splunk-confluence-modal" class="modal" style="margin-left: -400px; height: 80%; max-width: 800px; width:800px; display: none;">\n' +
    '<div class="modal-header">' +
    '   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>' +
    '   <h2 class="modal-title" style="float: left">Query finder</h2>' +
    '</div>' +
    '<div style="padding: 10px 20px 0px 20px;" class="form-group">' +
    '   <label for="title_filter">Filter queries by content (title, link, query)</label>\n' +
    '   <input class="mousetrap" type="text" class="form-control" id="title_filter" placeholder="Search...">\n' +
    '</div>' +
    '<div id="confluence_links" class="pre-scrollable" style="padding: 0px 10px; max-height: 70%">' +
    '</div>');

function isModalVisible() {
    return $('#splunk-confluence-modal').is(':visible');
}

$("#splunk-confluence-modal").on('click', function () {
    $("#title_filter").focus();
});

function toggleModal() {
    if (!isModalVisible()) {
        $('#splunk-confluence-modal').modal();
        setTimeout(function () {
            $("textarea.ace_text-input").blur();
            $("#title_filter").focus();
        }, 200);
    } else {
        $('#splunk-confluence-modal .close').click();
        $("textarea.ace_text-input").focus();
    }
}


// --------------------------------- END OF MODAL CREATION ----------------------------

$(function () {



    waitUntilExists(iconSelector, function () {
        Mousetrap.bind(['shift shift'], function (e) {
            toggleModal();
        });
    });

    observeUIChanges();

});

// --------------------------------- NAVIGATION UI CREATION ----------------------------

(function (history) {
    var pushState = history.pushState;
    history.pushState = function (state) {
        if (typeof history.onpushstate == "function") {
            history.onpushstate({state: state});
        }
        return pushState.apply(history, arguments);
    }
})(window.history);

function bindClickToOpenModal() {
    $('.query-archive-opener').on('click', function (e) {
        toggleModal();
        return false;
    });
}

function initUIIfNeeded() {

    if (!$('.search-name #splunk-query-finder-logo').length) {
        $(iconSelector).append(
            '<a href="#splunk-confluence-modal" class="query-archive-opener">' +
            '<img id="splunk-query-finder-logo"' +
            'src="' + $("#query-finder-logo-url").attr("url") + '" ' +
            'id="confluence_icon"></a>');

        $('<a href="#" class="btn-pill query-archive-opener">Query finder</a>')
            .insertAfter('.shared-jobstatus-samplingmode-menu');

        setTimeout(function () {
            $("textarea.ace_text-input").addClass("mousetrap");
        }, 500);

        bindClickToOpenModal();

    };
}



// --------------------------------- END OF NAVIGATION CREATION ----------------------------

// --------------------------------- ARROWS SUPPORT ----------------------------

$("#title_filter").on('keyup', function () {
    let searchString = $('#title_filter').val();
    if (currentSearchPhrase == searchString) {
        return;
    }
    currentSearchPhrase = searchString
    $('#confluence_links div').hide();
    let jqueryString = searchString.split(" ").filter(s => !isBlank(s)).map(s => ':contains(' + s + ')').join("");
    $('#confluence_links div' + jqueryString).show();
    resetHighlights();
});

$(document).on('keydown', function (event) {
    switch (event.key) {
        case "Down": // IE/Edge specific value
        case "ArrowDown":
            if (isModalVisible()) {
                event.preventDefault();
                highlightNext();
            }
            break;
        case "Up": // IE/Edge specific value
        case "ArrowUp":
            if (isModalVisible()) {
                event.preventDefault();
                highlightPrev();
            }
            break;
        case "Enter":
            if (isModalVisible()) {
                goToTheHighlightedLink();
                event.preventDefault();
            }
            break;
    }
});

function highlightPrev() {
    console.log("Highlight prev");
    if ($(highlightedClassSelector).length && $(highlightedClassSelector).prevAll("div:visible:first").length > 0) {
        $(highlightedClassSelector).removeClass(highlightedClass).prevAll("div:visible:first").addClass(highlightedClass)
    }
    updateScroll();
}

function highlightNext() {
    console.log("Highlight next");
    if ($(highlightedClassSelector).length) {
        //highlighted last element
        if ($(highlightedClassSelector).nextAll("div:visible:first").length === 0) {
            return;
        }
        $(highlightedClassSelector).removeClass(highlightedClass).nextAll("div:visible:first").addClass(highlightedClass)
    } else {
        $('#confluence_links div:visible').first().addClass(highlightedClass);
    }
    updateScroll();
}

function updateScroll() {
    let scrollPosition = $('#confluence_links').scrollTop() + $(highlightedClassSelector).position().top - ($('#confluence_links').height() / 2);
    if (scrollPosition < 0) {
        scrollPosition = 0;
    }
    $('#confluence_links').scrollTop(scrollPosition);
}

function resetHighlights() {
    console.log("Resetting highlights");
    $("#confluence_links div").removeClass(highlightedClass);
    $('#confluence_links div:visible:first').addClass(highlightedClass);
}

function goToTheHighlightedLink() {
    let link = $(highlightedClassSelector).find("a").attr("href");
    loadUrlWithoutReloading(link)
}

function loadUrlWithoutReloading(link) {
    window.history.pushState({},"History", link);
    window.history.back();

    setTimeout(function () {
        window.history.forward();
    }, 500);

    toggleModal();
    $('#confluence_links div').show();
    $('#title_filter').val("");
    resetHighlights();
}

function observeUIChanges() {
    setInterval(function () {
        initUIIfNeeded();
    }, 500);
}

// --------------------------------- END OF ARROWS SUPPORT ----------------------------
