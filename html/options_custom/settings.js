window.addEvent("domready", function () {
    // Option 1: Use the manifest:
    new FancySettings.initWithManifest(function (settings) {
        settings.manifest.myDescription.addEvent("action", function () {
            alert("You clicked me!");
        });
    });
});
